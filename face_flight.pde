import processing.video.*;
Capture cam;


PImage canvasState;
int[] nextState;
int H;
int W;
boolean firstGo;
int introTime = 10*60;
int secondIntroTime = 60;
int windowSize = 256;

Flock flock;
ArrayList<Boid> boids;
SpatialIndex<Attractor> repulsors;
ArrayList<PVector> droplets;

void setup() {
  size(256, 256);

  String[] cameras = Capture.list();

  if (cameras.length == 0) {
    println("There are no cameras available for capture.");
    exit();
  }

  cam = new Capture(this, 256, 256);
  cam.start();

  //W = cam.width;
  //H = cam.height;


  frameRate(2);
  firstGo = true;

  flock = new Flock(256, 256);
  repulsors = new SpatialIndex<Attractor>(256, 256);
  droplets = new ArrayList<PVector>();
}

void draw() {

  if (firstGo) {
    if (cam.available()) {
      cam.read();

      cam.loadPixels();
      cam.stop();

      blackWhiteRaster(windowSize, cam);
      firstGo = false;
      image(cam, 0, 0);
      frameRate(60);
    }
    return;
  } else if (introTime > 0) {
    introTime--;
    return;
  } else {
    background(#a6cfdd);
    for (PVector d : droplets) {
      pushStyle();
      stroke(#ffffff);
      point(d.x, d.y);
      popStyle();
    }
    for (Attractor r : repulsors) {
      r.draw();
    }
    flock.run(repulsors);
  }


  //  canvasState.updatePixels();
}

PImage blackWhiteRaster(int maxPixel, PImage img) {
  img.filter(GRAY);
  int w, h;
  if (img.width > img.height) {
    w = maxPixel;
    h = img.height * maxPixel / img.width;
  } else {
    w = img.width * maxPixel / img.height;
    h = maxPixel;
  }
  img.resize(w, h);
  img.loadPixels();

  float darkest = luminance(#ffffff);
  float brightest = luminance(#000000);


  for (int i = 0; i < img.pixels.length; i++) {
    if (luminance(img.pixels[i]) < darkest) {
      darkest = luminance(img.pixels[i]);
    }

    if (luminance(img.pixels[i]) > brightest) {
      brightest = luminance(img.pixels[i]);
    }
  }

  float middle = (brightest - darkest)/2;

  for (int i = 0; i < img.pixels.length; i++) {
    int x = i%w;
    int y = i/w;
    float l = luminance(img.pixels[i]);
    float dbrt = (float)Math.abs(l - brightest);
    float dmid = (float)Math.abs(l - middle);
    float ddrk = (float)Math.abs(l- darkest);
    color c = #a6cfdd;
    if (dbrt < dmid) {
      if (Math.random() > 0.95) {
        repulsors.add(new Attractor(new PVector(x, y), -5.0));
      }
      droplets.add(new PVector(x, y));
      c = #ffffff;
    } else if (ddrk < dmid) {
      if (Math.random() > 0.97) {
        flock.addBoid(new Boid(x, y, 1.0));
      }
      c = #000000;
    }

    img.pixels[i] = c;
  }
  img.updatePixels();
  return img;
}

float luminance(color c) {
  float r = red(c)/255.0;
  float g = green(c)/255.0;
  float b = blue(c)/255.0;
  return (0.299*r + 0.587*g + 0.114*b);
}
/*
void mouseClicked() {
 shouldCapture = true;
 }*/