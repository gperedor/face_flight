import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;

interface PElement {
  int getX();
  int getY();
}

class SpatialIndex<T extends PElement>  implements Iterable<T> {
  ArrayList<Set<T>> index;
  int w, h;

  SpatialIndex(int w, int h) {
    this.w = w;
    this.h = h;
    index = new ArrayList<Set<T>>();
    for (int i = 0; i < 8; i++) {
      index.add(new HashSet<T>());
    }
  }

  void add(T elem) {
    int base = Math.min(elem.getY()/(h/2), 1);
    int offset = Math.min(elem.getX()/(w/4), 3);
    index.get(4*base + offset).add(elem);
    println(elem.getX() + " " + elem.getY() + " " + (4*base + offset));
  }

  Set<T> getSet(int x, int y) {
    // Wraparound
    int wx = Math.max(x % w, 0);
    int wy = Math.max(y % h, 0);

    int base = Math.min(wy/(h/2), 1);
    int offset = Math.min(wx/(w/4), 3);
    //println(x + " " + y + " " + base + " " + offset);
    return index.get(4*base + offset);
  }

  boolean isEmpty() {
    for (Set<T> s : index) {
      if (!s.isEmpty()) return false;
    }
    return true;
  }

  Set<T> getAll() {
    Set<T> ts = new HashSet<T>();
    for (Set<T> s : index) {
      ts.addAll(s);
    }
    return ts;
  }
  
  Iterator<T> iterator() {
    return new SpatialIndexIterator(index);
  }
}

class SpatialIndexIterator<T extends PElement> implements Iterator<T> {
  ArrayList<Set<T>> index;
  Iterator<T> curTIterator;
  Iterator<Set<T>> curSetIterator;

  SpatialIndexIterator(ArrayList<Set<T>> index) {
    this.index = index;
    curSetIterator = index.iterator();
    if (curSetIterator.hasNext()) curTIterator = curSetIterator.next().iterator();
  }

  T next() {
    if (curTIterator == null) return null;
    if (curTIterator.hasNext()) return curTIterator.next();
    while (curSetIterator.hasNext() && !curTIterator.hasNext()) {
      Set<T> maybeSet = curSetIterator.next();
      if (maybeSet != null) curTIterator = maybeSet.iterator();
    }
    return curTIterator != null ? curTIterator.next() : null;
  }

  boolean hasNext() {
    if (curTIterator == null) return false;
    boolean had = curTIterator.hasNext();
    while (curSetIterator.hasNext() && !had) {
      Set<T> maybeSet = curSetIterator.next();
      if (maybeSet != null) { 
        curTIterator = maybeSet.iterator();
        had = curTIterator.hasNext();
      }
    }
    return had;
  }
}