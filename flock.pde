class Attractor implements PElement {
  PVector position;
  float intensity;

  int getX() {
    return (int)position.x;
  }

  int getY() {
    return (int)position.y;
  }

  Attractor(PVector position, float intensity) {
    this.position = position;
    this.intensity = intensity;
  }

  PVector attractionAt(PVector p) {
    float mag = (float)(1e10 * intensity/(Math.pow(position.dist(p), 2)));
    return position.copy().sub(p).setMag(mag);
  }

  void draw() {
    //pushStyle();
    fill(#ffffff);
    stroke(#ffffff);
    point(position.x, position.y);
    //popStyle();
  }
}

Attractor ZERO = new Attractor(new PVector(0, 0), 0.0);
boolean alreadyChosen = false;
class Flock {
  SpatialIndex<Boid> boids; // An ArrayList for all the boids

  Flock(int w, int h) {
    boids = new SpatialIndex<Boid>(w, h);
  }

  void run(SpatialIndex<Attractor> attractors) {
    for (Boid b : boids) {
      b.run(boids, attractors);  // Passing the entire list of boids to each boid individually
    }
  }

  void addBoid(Boid b) {
    boids.add(b);
  }
}

// The Boid class

class Boid implements PElement {

  boolean theOne;
  PVector position;
  PVector velocity;
  PVector acceleration;
  float r;
  float maxforce;    // Maximum steering force
  float maxspeed;    // Maximum speed

  Boid(float x, float y, float size) {
    acceleration = new PVector(0, 0);

    // This is a new PVector method not yet implemented in JS
    velocity = PVector.random2D();

    // Leaving the code temporarily this way so that this example runs in JS
    //float angle = random(TWO_PI);
    //velocity = new PVector(cos(angle), sin(angle));

    position = new PVector(x, y);
    r = size;
    maxspeed = 2;
    maxforce = 0.03;
    if (!alreadyChosen) {
      theOne = true;
      alreadyChosen = true;
    }
  }

  void run(SpatialIndex<Boid> boids, SpatialIndex<Attractor> attractors) {
    flock(boids, attractors);
    update();
    borders();
    render();
  }

  void applyForce(PVector force) {
    // We could add mass here if we want A = F / M
    acceleration.add(force);
  }

  // We accumulate a new acceleration each time based on three rules
  void flock(SpatialIndex<Boid> boids, SpatialIndex<Attractor> attractors) {
    PVector sep = separate(boids);   // Separation
    PVector ali = align(boids);      // Alignment
    PVector coh = cohesion(boids);   // Cohesion
    PVector tar;
    if (attractors.isEmpty()) tar = ZERO.attractionAt(position);
    else tar = seek(nearest(attractors).attractionAt(position));
    // Arbitrarily weight these forces
    sep.mult(1.5);
    ali.mult(1.0);
    coh.mult(1.1);
    tar.mult(1.0);
    if (theOne) {
      //println(tar.x + " " + tar.y);
    }
    // Add the force vectors to acceleration
    applyForce(sep);
    applyForce(ali);
    applyForce(coh);
    applyForce(tar);
  }

  // Method to update position
  void update() {
    // Update velocity
    velocity.add(acceleration);
    // Limit speed
    velocity.limit(maxspeed);
    position.add(velocity);
    // Reset accelertion to 0 each cycle
    acceleration.mult(0);
  }

  // A method that calculates and applies a steering force towards a target
  // STEER = DESIRED MINUS VELOCITY
  PVector seek(PVector target) {
    PVector desired = PVector.sub(target, position);  // A vector pointing from the position to the target
    // Scale to maximum speed

    desired.setMag(maxspeed);

    // Above two lines of code below could be condensed with new PVector setMag() method
    // Not using this method until Processing.js catches up
    // desired.setMag(maxspeed);

    // Steering = Desired minus Velocity
    PVector steer = PVector.sub(desired, velocity);
    steer.limit(maxforce);  // Limit to maximum steering force
    return steer;
  }

  void render() {
    // Draw a triangle rotated in the direction of velocity
    float theta = velocity.heading() + radians(90);
    // 18 40 60
    pushStyle();
    fill(#000000, 100);
    stroke(0);
    pushMatrix();
    translate(position.x, position.y);
    rotate(theta);
    beginShape(TRIANGLES);
    vertex(0, -r*2);
    vertex(-r, r*2);
    vertex(r, r*2);
    endShape();
    popMatrix();
    popStyle();
  }

  // Wraparound
  void borders() {
    if (position.x < -r) position.x = width+r;
    if (position.y < -r) position.y = height+r;
    if (position.x > width+r) position.x = -r;
    if (position.y > height+r) position.y = -r;
  }

  // Separation
  // Method checks for nearby boids and steers away
  PVector separate (SpatialIndex<Boid> boids) {
    float desiredseparation = 25.0f;
    PVector steer = new PVector(0, 0, 0);
    int count = 0;
    // For every boid in the system, check if it's too close
    for (Boid other : boids.getSet(getX(), getY())) {
      float d = PVector.dist(position, other.position);
      // If the distance is greater than 0 and less than an arbitrary amount (0 when you are yourself)
      if ((d > 0) && (d < desiredseparation)) {
        // Calculate vector pointing away from neighbor
        PVector diff = PVector.sub(position, other.position);
        diff.normalize();
        diff.div(d);        // Weight by distance
        steer.add(diff);
        count++;            // Keep track of how many
      }
    }
    // Average -- divide by how many
    if (count > 0) {
      steer.div((float)count);
    }

    // As long as the vector is greater than 0
    if (steer.mag() > 0) {
      // First two lines of code below could be condensed with new PVector setMag() method
      // Not using this method until Processing.js catches up
      // steer.setMag(maxspeed);

      // Implement Reynolds: Steering = Desired - Velocity
      steer.normalize();
      steer.mult(maxspeed);
      steer.sub(velocity);
      steer.limit(maxforce);
    }
    return steer;
  }

  // Alignment
  // For every nearby boid in the system, calculate the average velocity
  PVector align (SpatialIndex<Boid> boids) {
    float neighbordist = 50;
    PVector sum = new PVector(0, 0);
    int count = 0;
    for (Boid other : boids.getSet(getX(), getY())) {
      float d = PVector.dist(position, other.position);
      if ((d > 0) && (d < neighbordist)) {
        sum.add(other.velocity);
        count++;
      }
    }
    if (count > 0) {
      sum.div((float)count);
      // First two lines of code below could be condensed with new PVector setMag() method
      // Not using this method until Processing.js catches up
      // sum.setMag(maxspeed);

      // Implement Reynolds: Steering = Desired - Velocity
      sum.normalize();
      sum.mult(maxspeed);
      PVector steer = PVector.sub(sum, velocity);
      steer.limit(maxforce);
      return steer;
    } else {
      return new PVector(0, 0);
    }
  }

  // Cohesion
  // For the average position (i.e. center) of all nearby boids, calculate steering vector towards that position
  PVector cohesion (SpatialIndex<Boid> boids) {
    float neighbordist = 50;
    PVector sum = new PVector(0, 0);   // Start with empty vector to accumulate all positions
    int count = 0;
    for (Boid other : boids.getSet(getX(), getY())) {
      float d = PVector.dist(position, other.position);
      if ((d > 0) && (d < neighbordist)) {
        sum.add(other.position); // Add position
        count++;
      }
    }
    if (count > 0) {
      sum.div(count);
      return seek(sum);  // Steer towards the position
    } else {
      return new PVector(0, 0);
    }
  }

  Attractor nearest(SpatialIndex<Attractor> attractors) {
    Attractor nst = ZERO;
    float minDist = MAX_FLOAT;
    Set<Attractor> ats = attractors.getSet((int)position.x, (int)position.y);
    for (Attractor a : ats) {
      float d = a.position.dist(position);
      if (d < minDist) {
        minDist = d;
        nst = a;
      }
    }
    return nst;
  }

  int getX() {
    return (int)position.x;
  }

  int getY() {
    return (int)position.y;
  }
}